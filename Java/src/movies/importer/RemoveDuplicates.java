package movies.importer;

import java.util.ArrayList;

public class RemoveDuplicates extends Processor {

	public RemoveDuplicates(String sourceDir, String outputDir) {
		
		super(sourceDir, outputDir, false);
		
	}
	
	public ArrayList<String> process(ArrayList<String> input) {
		
		ArrayList<String> noDuplicate = new ArrayList<String>();
		String searchString;
		for(int i = 0; i < input.size(); i++) {
			
			searchString = input.get(i);
			if(!(noDuplicate.contains(searchString))) {
				
				noDuplicate.add(input.get(i));
				
			}
			
		}
		return noDuplicate;
		
	}
	
}
