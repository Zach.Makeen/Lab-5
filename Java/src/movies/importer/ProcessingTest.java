package movies.importer;

import java.io.IOException;

public class ProcessingTest {

	public static void main(String[] args) throws IOException {

		String source = "C:\\Users\\zacha\\Desktop\\fileList";
		String destination = "C:\\Users\\zacha\\Desktop\\destination";
		LowercaseProcessor lowercase = new LowercaseProcessor(source, destination);
		lowercase.execute();
		
		String secondDestination = "C:\\Users\\zacha\\Desktop\\duplicateRemoved";
		RemoveDuplicates noDuplicate = new RemoveDuplicates(destination, secondDestination);
		noDuplicate.execute();
		
	}

}
